<?php declare(strict_types=1);

use JTL\Backend\AdminAccountManager;

require_once __DIR__ . '/includes/admininclude.php';

$oAccount->permission('ACCOUNT_VIEW', true, true);

/** @global \JTL\Smarty\JTLSmarty $smarty */
$adminAccountManager = new AdminAccountManager($smarty, Shop::Container()->getDB());
$adminAccountManager->finalize($adminAccountManager->getNextAction());
